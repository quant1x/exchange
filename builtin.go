package exchange

// 历史成交数据 historical transaction data
const (
	HistoricalTransactionDataFirstTime        = "09:25" // 第一个时间
	HistoricalTransactionDataStartTime        = "09:30" // 开盘时间
	HistoricalTransactionDataFinalBiddingTime = "14:57" // 尾盘集合竞价时间
	HistoricalTransactionDataLastTime         = "15:00" // 最后一个时间
)
